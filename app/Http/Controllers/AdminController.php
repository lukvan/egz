<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\User;
use App\Post;

class AdminController extends Controller
{

    public function index ()
    {
        $id = Auth::user()->id;
        $posts = Post::where('user_id', $id)->get();

        return view('admin.profile', compact('posts'));
    }
}
