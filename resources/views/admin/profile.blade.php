@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Tavo Įrašai:</div>

                    <div class="panel-body">

                        <table class="table">
                            <tr>
                                <th>Pavadinimas</th>
                                <th>Kategorija</th>
                                <th>Pasirinkimai</th>
                            </tr>
                            @foreach($posts as $post)
                                <tr>
                                    <td><a href="{{ url('/posts', $post->id) }}"><h2>{{ $post->title }}</h2></a></td>
                                    <td><h2>{{ $post->category }}</h2></td>
                                    <td>
                                        <form action="/posts/{{ $post->id }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-flat btn-danger">Ištrinti</button>
                                            <input type="button" class="btn btn-flat btn-success" value="Redaguoti" onclick ="window.location.href='/posts/{{$post->id}}/edit'" />
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                </div>
                    <input type="button" class="btn btn-flat btn-primary" value="Naujas įrašas" onclick="window.location.href='/posts/create'" />

            </div>
        </div>
    </div>
@endsection
