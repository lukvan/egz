@extends('layouts.main')


@section('content')

    @foreach($posts as $category)
        <div class="col-md-4">
        <a href="{{ url('/', $category->category) }}">
            <p>{{ $category->category }}</p></a>
        </div>
    @endforeach
    <div class="row">
    @foreach ($posts as $post)
    <div class="col-md-4">
        <a href="{{ url('/posts', $post->id) }}">
        <h2>{{ $post->title }}</h2></a>

        <p>{{str_limit($post->description, 100)}}</p>
        <p><a class="btn btn-default" href="/posts/{{$post->id}}" role="button">Daugiau</a></p>
    </div>
    @endforeach
    </div>
    {{ $posts->links() }}

@endsection
