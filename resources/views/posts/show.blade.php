@extends('layouts.main')


@section('content')

    <h2>{{ $post->title }}</h2>
    <p>{{ $post->description }}</p>

    @if(Auth::id() == $post->user_id)
        <form action="/posts/{{ $post->id }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-flat btn-danger">Ištrinti</button>
            <input type="button" class="btn btn-flat btn-success" value="Redaguoti" onclick="window.location.href='/posts/{{$post->id}}/edit'" />
        </form>
    @endif

@endsection
